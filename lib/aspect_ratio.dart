import 'package:flutter/material.dart';

void main(){

}

class AspectRatioexample extends StatelessWidget {
  const AspectRatioexample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(50.0),
      child: Center(
        child: AspectRatio(
          aspectRatio: 1.5,
          child: Container(
            color: Colors.green[200],
            child: const FlutterLogo(),
          ),
        ),
      ),
    );
  }
}
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {


 static PreferredSizeWidget buildAppbar(BuildContext context){
    return AppBar(

      title: Text("Weather"),
      actions: [
        IconButton(onPressed: () {
          Navigator.pushNamed(context, '/setting');
        }, icon: Icon(Icons.settings))
      ],
    );
  }
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Weather App',
      theme: ThemeData(
          // appBarTheme: AppBarTheme(
          //   color: Colors.black12
          // ),
          // primarySwatch: Colors.blue,
          brightness: Brightness.dark
      ),
      initialRoute: '/',
      routes: {
        '/': (_) => HomeScreen(),
        '/setting': (_) => SettingScreen(),
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: Text("Weather"),
        //   actions: [
        //     IconButton(onPressed: () {
        //       Navigator.pushNamed(context, '/setting');
        //     }, icon: Icon(Icons.settings))
        //   ],
        // ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              Colors.grey.shade700,
              Colors.black
            ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter
            ),

          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 30 ,right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _cloudIcon(),
                _temperature(),
                _location(),
                _hourlyPrediction(),
                _weeklyPrediction(),
              ],
            ),
          ),
        )
    );
  }
  _cloudIcon(){
    return Padding(
      padding: const EdgeInsets.all(28.0),
      child: Icon(Icons.cloud, size: 120,),
    );
  }

  _temperature() {
    return Text("25º", style: TextStyle(fontSize: 80, fontWeight: FontWeight.w100),);
  }

  _location() {
    return Row(
      children: [
        Icon(Icons.place),
        SizedBox(
          width: 10,
        ),
        Text("London, Westminster"),
      ],
    );
  }
  final times = ["8","9","10","11","12","13","14","15","16","17","18"];
  final hour_temp = ["23º","23º","24º","25º","26º","27º","28º","29º","28º","27º","26º"];
  _hourlyPrediction() {
    return Container(
      height: 100,
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(color:  Colors.white),
              bottom: BorderSide(color: Colors.white)
          )
      ),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: times.length,
          itemBuilder: (context, index){
            return Container(
              width: 50,
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("${times[index]}"),
                    (index >2 && index <7) ?  Icon(Icons.sunny): Icon(Icons.cloud),
                    Text("${hour_temp[index]}"),
                  ],
                ),
                // Center(
                //   child: Text("${times[index]}"),
                // ),
              ),
            );
          }),
    );
  }
  final weeks =  ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  final day_temp = ["24º","25º","25º","27º","28º","29º","30º"];
  _weeklyPrediction() {
    return Expanded(
      child: Container(
        height: 100,
        child: ListView.builder(
            shrinkWrap: false,
            scrollDirection: Axis.vertical,
            itemCount: weeks.length,
            itemBuilder: (context, index){
              return Container(
                height: 50,
                child: Card(
                  child:  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(child: Text("${weeks[index]}"),flex: 2,),
                      Expanded(child: (index >3 && index <7) ?  Icon(Icons.sunny): Icon(Icons.cloud),flex: 6),
                      Expanded(child: Text("${day_temp[ index]}"),flex: 1),
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }

}
class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("setting"),

      ),
      body: Container(
          child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon((Icons.construction), size: 80,),
                  Text("This Page is not available yet.",style: TextStyle(fontSize: 30),)
                ],
              )
          )

      ),
    );
  }
}
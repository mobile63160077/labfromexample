import 'package:device_preview/device_preview.dart';
import 'package:example2/aspect_ratio.dart';
import 'package:example2/weather_app.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
          appBarTheme: AppBarTheme(
              color: Colors.black54
          ),
        ),
        home: Scaffold(
          // backgroundColor: Colors.white,
          appBar: MyApp.buildAppbar(context),
          body: MyApp()
        ),
      ),
    );
  }
}

